(function ($) {
    $.fn.showHide = function (options) {
        var defaults = {
            speed: 1000,
            easing: '',
        };
        var options = $.extend (defaults, options);

        $(this).click (function () {
            var clicked = $(this);
            var todisplay = "#" + $(this).attr('data-rel');

            $(todisplay).slideToggle(options.speed, options.easing);

            if (clicked.hasClass ('toggled'))
                clicked.removeClass ('toggled').addClass ('expanded');
            else
                clicked.removeClass ('expanded').addClass ('toggled');

            return true;
        });
    };
})(jQuery);

$(document).ready (function () {
    $('.toggler').showHide ( {
        speed: 250,
        easing: '',
    });
});

<?php
function getFileFormattedPath ($path) {
    return preg_replace ('#/#is', DIRECTORY_SEPARATOR, $path);
}

function getURLFormattedPath ($path) {
    return preg_replace ('#' . DIRECTORY_SEPARATOR . '#is', '/', $path);
}
?>

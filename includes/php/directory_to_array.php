<?php
/**
 * Get an array that represents directory tree
 * @param string $directory         Directory path
 * @param bool $recursive           Include sub directories
 * @param bool $listDirs            Include directories on listing
 * @param bool $listFiles           Include files on listing
 * @param regex $exclude            Exclude paths that matches this regex
 */
function directoryToArray($directory, $recursive = true, $listDirs = true, $listFiles = true, $exclude = '') {
    $arrayItems = array();
    $skipByExclude = false;
    $handle = opendir($directory);
    if ($handle) {
        while (false !== ($file = readdir($handle))) {
            preg_match("/(^(([\.]){1,2})$|(\.(svn|git|md))|(Thumbs\.db|\.DS_STORE))$/iu", $file, $skip);
            if($exclude){
                preg_match($exclude, $file, $skipByExclude);
            }
            if (!$skip && !$skipByExclude) {
                if (is_dir($directory. DIRECTORY_SEPARATOR . $file)) {
                    if($recursive) {
                        $arrayItems[$file] = directoryToArray($directory. DIRECTORY_SEPARATOR . $file, $recursive, $listDirs, $listFiles, $exclude);
                    }
                    else if($listDirs){
                        $arrayItems[$file] = $directory . DIRECTORY_SEPARATOR . $file;
                    }
                }
                else {
                    if($listFiles){
                        $arrayItems[$file] = $directory . DIRECTORY_SEPARATOR . $file;
                    }
                }
            }
        }
        closedir($handle);
    }
    uasort($arrayItems, 'sortDirectoriesAndFiles');
    return $arrayItems;
}
?>

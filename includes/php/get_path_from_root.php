<?php
function getPathFromRoot () {
    static $folder = null;

    if ($folder == null) {
        $folder = preg_replace ('#' . $_SERVER['DOCUMENT_ROOT'] . '#is', '', $_SERVER['SCRIPT_FILENAME']);
        $folder = preg_replace ('#^/(.*/?)[^/]*$#isU', '$1', $folder);
    }

    return $folder;
}
?>

<?php
require_once ('conf.php');
require_once ('print_tabs.php');
require_once ('path_formatter.php');
require_once ('dir_saver.php');
require_once ('icon_getter.php');
require_once ('directory_to_array.php');
require_once ('get_path_from_root.php');

function getPathToRoot () {
    static $path = null;

    if ($path == null) {
        $path = '.';
        $count = preg_match_all ('#' . DIRECTORY_SEPARATOR . '#is', getPathFromRoot ());
        for ($i = 0; $i < $count; $i++) {
            $path .= '/..';
        }
    }

    return $path;
}

saveDir ();
chdir (getFileFormattedPath (getPathToRoot () . $_SERVER['REQUEST_URI']));

function sortDirectoriesAndFiles ($a, $b) {
    if (is_array ($a) && is_array ($b)) {
        return (sortDirectoriesAndFiles (reset ($a), reset ($b)));
    }
    else if (is_array ($a))
        return 1;
    else if (is_array ($b))
        return -1;
    else if ($a == $b)
        return 0;
    else
        return ($a < $b) ? -1 : 1;
}


function _createArborescence () {
    static $directory = null;

    if ($directory == null)
        $directory = '.';

    echo iEndl () . '<ul>' . incIndent ();
    foreach (directoryToArray($directory, false, false, true) as $k => $v) {
        echo iEndl () . '<li class="file">';
        echo incEndl () . '<a href="' . $v . '">';
        echo incEndl () . '<img src="/' . getPathFromRoot () . 'design/img/filetype-icons/' . getIcon ($v) . '.png" alt="[File] "/>';
        echo iEndl () . $k;
        echo decEndl () . '</a>';
        echo decEndl () . '</li>';
    }

    foreach (directoryToArray($directory, false, true, false, INDEXOF_EXCLUDE_REGEX) as $k => $v) {
        $link = preg_replace ('#/|\.#is', '-', $v);
        echo iEndl () . '<li class="folder toggler toggled" data-rel="' . $link . '">';
        echo incEndl () . '<a href="' . $v . '">';
        echo incEndl () . '<img src="/' . getPathFromRoot () . 'design/img/filetype-icons/folder.png" alt="[Folder] "/>';
        echo iEndl () . $k . '';
        echo decEndl () . '</a>';
        echo decEndl () . '</li>';
        echo iEndl () . '<li id="' . $link . '" style="display: none;">';
        incIndent ();
        $directory = $v;
        _createArborescence ();
        decIndent ();
        echo iEndl () . '</li>';
    }
    echo decEndl () . '</ul>';
}

function createArborescence () {

    currentIndent (3);
    echo '<ul>';
    if ($_SERVER['REQUEST_URI'] != "/") {
        echo incEndl () . '<li class="folder">';
        echo incEndl () . '<a href="..">';
        echo incEndl () . '<img src="/' . getPathFromRoot () . 'design/img/filetype-icons/folder.png" alt="[Folder] "/>';
        echo iEndl () . '..';
        echo decEndl () . '</a>';
        echo decEndl () . '</li>';
    }

    echo incEndl () . '<li class="folder toggler expanded" data-rel="-">';
    echo incEndl () . '<a href=".">';
    echo incEndl () . '<img src="/' . getPathFromRoot () . 'design/img/filetype-icons/folder.png" alt="[Folder] "/>';
    echo iEndl () . '.';
    echo decEndl () . '</a>';
    echo decEndl () . '</li>';
    echo iEndl () . '<li id="-">';
    incIndent ();
    _createArborescence ();
    decIndent ();
    echo iEndl () . '</li>';
    echo decEndl () . '</ul>';

    restoreDir ();
}
?>

<?php
function saveDir ($save = true) {
    static $dir = null;

    if ($save)
        $dir = getcwd ();
    else
        return $dir;
}

function getSavedDir () {
    return saveDir (false);
}

function restoreDir () {
    chdir (getSavedDir ());
}
?>

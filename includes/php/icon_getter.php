<?php

require_once ('directory_to_array.php');
require_once ('dir_saver.php');

function getAvailableFileTypes () {
    static $extensions = null;

    if ($extensions == null) {
        $path =
        $extensions = array_keys (directoryToArray (getSavedDir () . '/design/img/filetype-icons/', false, false, true));

        foreach ($extensions as $k => $v) {
            $extensions[$k] = preg_replace('#\.png$#', '', $v);
        }
    }

    return $extensions;
}

function getIcon ($filename) {
    $icon = pathinfo($filename, PATHINFO_EXTENSION);
    $icon = ($icon === '' || !in_array ($icon, getAvailableFileTypes())) ? '_blank' : $icon;

    return $icon;
}
?>

<?php
/*
 * Functions for pretty formatting the HTML output.
 * - currentIdent sets (if one param) the current identation, else returns it.
 * - incIndent add one level of identation.
 * - decIndent removes one level of identation.
 * - incEndl add one level of indentation and goes to the next line
 * - decEndl removes one level of indentation and goes to the next line.
 *
 * Every function there can be inserted inside a string, (inc/dec) must be.
 */
function currentIndent ($new = -1) {
    static $n = 0;

    if ($new == -1)
        return $n;
    else
        $n = $new;
}

function getIndent () {
    $n = currentIndent ();

    echo "\n";

    for ($i = 0; $i < $n; $i++) {
        echo '    ';
    }
}

function incIndent () {
    currentIndent (currentIndent () + 1);
    return '';
}

function decIndent () {
    currentIndent (currentIndent () - 1);
    return '';
}

function incEndl () {
    incIndent ();
    return getIndent ();
}

function decEndl () {
    decIndent ();
    return getIndent ();
}

function iEndl () {
    return getIndent ();
}
?>

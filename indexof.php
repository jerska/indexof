<?php
require_once ('includes/php/scanner.php');
require_once ('conf.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Index of <?php echo getcwd(); ?>

        </title>

        <meta charset="utf-8" />

        <script type="text/javascript" src="http://code.jquery.com/jquery-2.0.0.js"></script>
        <script type="text/javascript" src="/<?php echo getURLFormattedPath (getPathFromRoot ()); ?>includes/js/toggler.js"></script>

        <link rel="stylesheet" type="text/css" href="/<?php echo getURLFormattedPath (getPathFromRoot ()); ?>design/default.css">
        <link rel="stylesheet" type="text/css" href="/<?php echo getURLFormattedPath (getPathFromRoot ()); ?>design/toggler.css">
    </head>
    <body>
        <div class="index">
            <h2>
                Index of <?php echo $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>

            </h2>
            <?php createArborescence (); ?>

        </div>
    </body>
</html>
